var express = require('express');
var router = express.Router();

var pg = require('pg');
var connectionString = "postgres://postgres:postgres@localhost/auradb";

var dataQuery = "select id from cler_suit_data";
var groupQuery = "select * from cler_suit_groups";
var userQuery = "select * from res_users";

/* GET home page. */
router.get('/', function(req, res, next) {
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  res.render('index', { title: 'Api Rest' });
  next();
});

router.get('/data', function(req, res){
  var client = new pg.Client(connectionString); 
  client.connect(); 
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(dataQuery, [], (err, result) => { 
    res.json(result.rows); 
    res.end(); 
    client.end() 
  });
});


// API PARA OBTENER LOS GRUPOS EN AURADB
router.get('/groups', function(req, res){
  var client = new pg.Client(connectionString); 
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Credentials: true');
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(groupQuery, [], (err, result) => { 
    res.json(result.rows); 
    res.end(); 
    client.end() 
  });  
});

// API PARA OBTENER LOS GRUPOS PARA EL NG-SELECT EN AURADB
router.get('/groupsNGS', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select id as value, name as label from cler_suit_groups";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Credentials: true');
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows); 
    res.end(); 
    client.end() 
  });  
});

// API PARA OBTENER LOS GRUPOS PARA EL NG-SELECT EN AURADB POR NOMBRE
router.get('/groupsNGS/:name', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select id as value, name as label from cler_suit_groups where name like '%"+req.params.name+"%'";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Credentials: true');
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows); 
    res.end(); 
    client.end() 
  });  
});

// API PARA OBTENER LOS GRUPOS PARA EL NG-SELECT EN AURADB POR NOMBRE
/*router.get('/groupsNGS/:name', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select id as value, name as label from cler_suit_groups where name like '%"+req.params.name+"%'";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Credentials: true');
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    if (err) {
      res.json({
        status: 0,
        message: err.message
      });
      res.end();
      client.end();
    }else{
      res.json({
        status: 1,
        message: 'Grupos encontrados con exito',
        total : result.rows.length,
        data: result.rows
      });
      res.end();
      client.end();
    }
  });  
});*/

//API PARA OBTENER TODOS LOS USUARIOS DE AURADB
router.get('/users', function(req, res){
  var client = new pg.Client(connectionString); 
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows); 
    res.end(); 
    client.end() 
  });  
});

//API PARA SABER SI EXISTE UN USUARIO EN AURADB
router.get('/user/:name', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQueryLogin = "select * from res_users where login='"+req.params.name+"'";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQueryLogin, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA DE CADA INTEGRANTE POR GRUPO EN AURADB
router.get('/dataGroups', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQueryLogin = "select * from cler_suit_data limit 700";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQueryLogin, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA DE COMPANIES
router.get('/dataCompanies', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select * from res_company";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OPTENER ID COMPANIES DE AURADB
router.get('/getCompanies/:id', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select * from res_company where id = '"+req.params.id+"'";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    if (err) {
      res.json({
        status: 0,
        message: err.message
      });
      res.end();
      client.end();
    }else{
      res.json({
        status: 1,
        message: 'Companies encontrada con exito',
        data: result.rows
      });
      res.end();
      client.end();
    }
  });  
});

//API PARA MODIFICAR COMPANIES DE AURADB
router.get('/putCompanies', function(req, res){
  var client = new pg.Client(connectionString); 
  var nameCompanie = req.param('name').toUpperCase();
  var emailCompanie = req.param('email');
  var phoneCompanie = req.param('phone');
  var userQuery = "update res_company set name = '"+nameCompanie+"' ," +
                  " email = '"+emailCompanie+"', phone = '"+phoneCompanie+"' "+
                  " where name = '" + nameCompanie + "'";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    if (err) {
      res.json({
        status: 0,
        message: err.message
      });
      res.end();
      client.end();
    }else{
      res.json({
        status: 1,
        message: 'Companies actualizada con exito'
      });
      res.end();
      client.end();
    }
  });  
});

//API PARA BORRAR ID COMPANIES DE AURADB
router.get('/deleteCompanies/:id', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "delete from res_company where id = '"+req.params.id+"'";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    if (err) {
      res.json({
        status: 0,
        message: err.message
      });
      res.end();
      client.end();
    }else{
      res.json({
        status: 1,
        message: 'Companies eliminada con exito'
      });
      res.end();
      client.end();
    }
  });  
});

//API PARA GUARDAR NUEVA COMPANIE EN AURADB
router.get('/setCompanies', function(req, res){
  var client = new pg.Client(connectionString); 
  var nameCompanie = req.param('name').toUpperCase();
  var emailCompanie = req.param('email');
  var phoneCompanie = req.param('phone');
  
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();
  var hora = hoy.getHours();
  var minuto = hoy.getMinutes();
  var secundo = hoy.getSeconds();
  dd = addZero(dd);
  mm= addZero(mm);
  var dateCompanie = yyyy+'-'+mm+'-'+dd+' '+hora+':'+minuto+':'+secundo;
  console.log(dateCompanie);

  var QuerySet = "insert into res_company (name,partner_id,email,phone,write_date) values ('"+nameCompanie+"','1','"+emailCompanie+"','"+phoneCompanie+"','"+dateCompanie+"')";
  
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(QuerySet, [], (err, result) => { 
    if (err) {
        res.json({
          status: 0,
          message: err.message
        });
        res.end();
        client.end();
    }else{
      res.json({
        status: 1,
        message: 'Insertado con exito'
      });
      res.end(); 
      client.end()
    }
  });
});

//API PARA OBTENER DATA DEL EMPLEADO
router.get('/dataEmployeexId/:id', function(req, res){
  var client = new pg.Client(connectionString);
  var id = req.params.id;
  var userQuery = "select * from hr_employee where res_user_id ="+id;  
  client.connect(); 
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA DEL EMPLEADO PARA LOS GRAFICOS
router.get('/dataEmployeeGraph/:name/:tipo/:num', function(req, res){
  var client = new pg.Client(connectionString);
  var name = req.params.name.toUpperCase();
  var tipo = req.params.tipo.toUpperCase();
  var num = req.params.num;
  var userQuery = "";
  if(tipo == "HOUR")        userQuery = "select distinct(suit_timestamp), s3_temp, s1_accel_z from cler_suit_data where name = '"+name+"' and DATE_PART('Day',now() - suit_timestamp::timestamptz) = 1 and DATE_PART('Hour',now() - suit_timestamp::timestamptz) < "+num+" order by suit_timestamp desc";
  else if (tipo == "DAY"){
    userQuery = "select distinct(suit_timestamp), s3_temp, s1_accel_z from " +
                " (select s3_temp, suit_timestamp, s1_accel_z " +
                " from cler_suit_data " +
                " where name = '"+name+"' " +
                " order by suit_timestamp desc) as x " +
                " where DATE_PART('Day',now() - suit_timestamp::timestamptz) <= "+num+"";
  }   
  else if (tipo == "MONTH") userQuery = "select distinct(suit_timestamp), s3_temp, s1_accel_z from cler_suit_data where name = '"+name+"' and DATE_PART('Month',now() - suit_timestamp::timestamptz) < "+num+" order by suit_timestamp desc";
  client.connect();
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA PARA COMPONENTE UBICACION
router.get('/dataUbicacion/:id', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select a.name,b.hr_employee_id,d.res_user_id, " +
    " (select id from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select s3_temp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select server_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select create_date from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select suit_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select sum(s4_lux) from cler_suit_data where name=a.name limit 10), " +
    " (select name from res_partner where user_id = d.res_user_id) as nombre " +
    " from cler_suit_group_line as b, cler_suit_employee as c, cler_suit_central as a, hr_employee d  " +
    " where b.cler_suit_groups_id = '"+req.params.id+"' and b.hr_employee_id =c.hr_employee_id and c.cler_suit_central_id = a.id and d.id = b.hr_employee_id";  
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA PARA COMPONENTE UBICACION POR NOMBRE
router.get('/dataUbicacionxNombre/:name', function(req, res){
  var client = new pg.Client(connectionString);
  var name = req.params.name.toUpperCase();
  var userQuery = "select a.name,b.hr_employee_id,d.res_user_id, " +
    " (select id from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select s3_temp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select server_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select create_date from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select suit_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select sum(s4_lux) from cler_suit_data where name=a.name limit 10), " +
    " (select name from res_partner where user_id = d.res_user_id) as nombre " +
    " from cler_suit_group_line as b, cler_suit_employee as c, cler_suit_central as a, hr_employee d  " +
    " where upper(d.name_related) like '"+name+"%' and b.hr_employee_id =c.hr_employee_id and c.cler_suit_central_id = a.id and d.id = b.hr_employee_id";  
  client.connect(); 
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA PARA COMPONENTE UBICACION POR NOMBRE TODOS ALL
router.get('/dataUbicacionxNombreAll', function(req, res){
  var client = new pg.Client(connectionString);
  //var name = req.params.name.toUpperCase();
  var userQuery = "select a.name,b.hr_employee_id,d.res_user_id, " +
    " (select id from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select s3_temp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select server_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select create_date from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select suit_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select sum(s4_lux) from cler_suit_data where name=a.name limit 10), " +
    " (select name from res_partner where user_id = d.res_user_id) as nombre " +
    " from cler_suit_group_line as b, cler_suit_employee as c, cler_suit_central as a, hr_employee d  " +
    " where d.name_related like '%%' and b.hr_employee_id =c.hr_employee_id and c.cler_suit_central_id = a.id and d.id = b.hr_employee_id";  
  client.connect(); 
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

//API PARA OBTENER DATA PARA COMPONENTE UBICACION DEPENDIENDO DEL GRUPO SELECCIONADO
router.get('/dataUbicacionComp/:id', function(req, res){
  var client = new pg.Client(connectionString); 
  var userQuery = "select a.name,b.hr_employee_id, d.res_user_id, " +
    " (select id from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select s3_temp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select s1_accel_z from cler_suit_data where name = a.name limit 1), " +
    " (select s1_accel_x from cler_suit_data where name = a.name limit 1), " +
    " (select s1_accel_y from cler_suit_data where name = a.name limit 1), " +
    " (select light_status from cler_suit_data where name = a.name limit 1)," +
    " (select s6_lux from cler_suit_data where name = a.name limit 1), " +
    " (select suit_state from cler_suit_data where name = a.name limit 1), " +
    " (select s4_lux from cler_suit_data where name = a.name limit 1), " +
    " (select s5_lux from cler_suit_data where name = a.name limit 1), " +
    " (select s8_gas from cler_suit_data where name = a.name limit 1), " +
    " (select s8_gas from cler_suit_data where name = a.name limit 1), " +
    " (select s10_tvoc from cler_suit_data where name = a.name limit 1), " +
    " (select s9_tvoc from cler_suit_data where name = a.name limit 1), " +
    " (select suit_id from cler_suit_data where name = a.name limit 1), " +
    " (select server_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select create_date from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select suit_timestamp from cler_suit_data where name=a.name order by id desc limit 1), " +
    " (select sum(s4_lux) from cler_suit_data where name=a.name limit 10), " +
    " (select name from res_partner where user_id = d.res_user_id) " +
    " from cler_suit_central as a, cler_suit_group_line as b, cler_suit_employee as c, hr_employee d " +
    " where b.cler_suit_groups_id = '"+req.params.id+"' and b.hr_employee_id = c.hr_employee_id and c.cler_suit_central_id = a.id " +
    " and d.id = b.hr_employee_id ";
  client.connect();     
  res.header('Access-Control-Allow-Origin',"*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','Content-Type');
  client.query(userQuery, [], (err, result) => { 
    res.json(result.rows);         
    res.end(); 
    client.end() 
  });  
});

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

module.exports = router;
